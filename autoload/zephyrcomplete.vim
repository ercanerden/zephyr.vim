"This program is free software: you can redistribute it and/or modify
"it under the terms of the GNU General Public License as published by
"the Free Software Foundation, either version 3 of the License, or
"(at your option) any later version.

"This program is distributed in the hope that it will be useful,
"but WITHOUT ANY WARRANTY; without even the implied warranty of
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"GNU General Public License for more details.

"You should have received a copy of the GNU General Public License
"along with this program.  If not, see <http://www.gnu.org/licenses/>.
"===============================================================================
"Ercan Erden - 2012
"ercerden@gmail.com
"http://www.bitbucket.org/ercax
"===============================================================================
function! zephyrcomplete#CompleteZephyr(findstart, base)
	if a:findstart == 1
		let line = getline('.')
		let start = col('.') - 1
		while start > 0  && line[start -1] =~ '\i\|'''
			let start -= 1
		endwhile
		return start
		echo line
	else
		let l:pattern = '^' . a:base . '.*$'
		for Tag_Item in g:zephyr#Keywords
			if l:Tag_Item['word'] =~? l:pattern
				if complete_add(l:Tag_Item) == 0
					return []
				endif
				if complete_check()
					return []
				endif
			endif
		endfor
		return []
	endif
endfunction

