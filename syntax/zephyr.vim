"This program is free software: you can redistribute it and/or modify
"it under the terms of the GNU General Public License as published by
"the Free Software Foundation, either version 3 of the License, or
"(at your option) any later version.

"This program is distributed in the hope that it will be useful,
"but WITHOUT ANY WARRANTY; without even the implied warranty of
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"GNU General Public License for more details.

"You should have received a copy of the GNU General Public License
"along with this program.  If not, see <http://www.gnu.org/licenses/>.
"===============================================================================
"Ercan Erden - 2012
"ercerden@gmail.com
"http://www.bitbucket.org/ercax
"===============================================================================

syn case match
runtime! syntax/html.vim

syn cluster ZephyrInner contains=zephyrBoolean,zephyrStatement,zephyrFunction,zephyrUserVariable,zephyrOperator,zephyrString,zephyrComment

syn keyword zephyrBoolean true false  contained

syn keyword zephyrStatement for in foreach as if else switch case include contained

syn keyword zephyrFunction abs ad adinfo api_send api_set_email api_user assert bucket_list \contained date filter first horizon_interest horizon_select horizon_set_interest insight int intersect keys length list lower map md5 number parse public_share random replace round setlocale signup_confirm slice social_share sort strip_tags substr time title type upper user_engagement user_geo_select_region values lambda contained

syn keyword zephyrUserVariable name email first_name last_name source postal_address postal_address2 postal_city postal_state postal_code country locale phone timezone gender birth_date marital sex_orientation ethnicity income education_level household_size children industry contained

syn keyword zephyrTemplateVariable beacon beacon_src beacon_url blast mail emailnum forward_url optout_confirm_url optout_client_url profile profile public_url signup_confirm_url text_url view_url contained

syn keyword zephyrMessageField template revision_id send_time open_time click_time purchase_time clicks opens sample contained
syn keyword zephyrProfileField id email lists lists_signup vars signup_time open_time click_time purchase_time purchases purchase_incomplete contained

syn match zephyrOperator "&&\|||\|==\|!=\|<=\|>=\|>\|<\|!\|+\|-\|*\|\/\|%\|?\|:\|=\|\." contained
syn region zephyrString matchgroup=Delimiter start=+'\|"+ end=+'\|"+ contained
syn match zephyrFunctionParen "[()]"

syn region zephyrZoneGenerate matchgroup=Title start="{" end="}" contains=@ZephyrInner fold
syn region zephyrZoneSend matchgroup=PreProc start="{{" end="}}" contains=@ZephyrInner fold
syn region zephyrComment start="{\*" end="\*}"  fold

hi link zephyrStatement Statement
hi link zephyrFunction Function
hi link zephyrDelimiterGenerate Delimiter
hi link zephyrOperator Operator
hi link zephyrBoolean Boolean
hi link zephyrString String
hi link zephyrTemplateVariable Special
hi link zephyrUserVariable Special
hi link zephyrComment Comment
hi link zephyrFunctionParen Delimiter

let b:current_syntax = "zephyr"
