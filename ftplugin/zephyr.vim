"This program is free software: you can redistribute it and/or modify
"it under the terms of the GNU General Public License as published by
"the Free Software Foundation, either version 3 of the License, or
"(at your option) any later version.

"This program is distributed in the hope that it will be useful,
"but WITHOUT ANY WARRANTY; without even the implied warranty of
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
"GNU General Public License for more details.

"You should have received a copy of the GNU General Public License
"along with this program.  If not, see <http://www.gnu.org/licenses/>.
"===============================================================================
"Ercan Erden - 2012
"ercerden@gmail.com
"http://www.bitbucket.org/ercax
"===============================================================================

setlocal completefunc=zephyrcomplete#CompleteZephyr 
runtime! ftplugin/html.vim ftplugin/html_*.vim ftplugin/html/*.vim

if exists(b:undo_ftplugin)
	let b:undo_ftplugin .= "|setlocal fo< com< flp< cms<" 
else
	let b:undo_ftplugin = "setlocal fo< com< flp< cms<" 
endif

let booleans = ['true', 'false']
let statements = ['for', 'in', 'foreach', 'as', 'else', 'switch', 'case', 'include', 'switch']
let functions = ['abs(', 'ad(', 'adinfo(', 'api_send(', 'api_set_email(', 'api_user(', 'assert(', 'bucket_list(', 'contained(', 'date(', 'filter(', 'first(', 'horizon_interest(', 'horizon_select(', 'horizon_set_interest(', 'insight(', 'int(', 'intersect(', 'keys(', 'lambda(', 'length(', 'list(', 'lower(', 'map(', 'md5(', 'number(', 'parse(', 'public_share(', 'random(', 'replace(', 'round(', 'setlocale(', 'signup_confirm(', 'slice(', 'social_share(', 'sort(', 'strip_tags(', 'substr(', 'time(', 'title(', 'type(', 'upper(', 'user_engagement(', 'user_geo_select_region(', 'values('] 
let templateVariables = ['beacon', 'beacon_src', 'beacon_url', 'blast', 'mail', 'emailnum', 'forward_url', 'optout_confirm_url', 'optout_client_url', 'profile', 'profile', 'public_url', 'signup_confirm_url', 'text_url', 'view_url']

let keywords = booleans + statements + functions + templateVariables
let g:zephyr#Keywords = []
for item in keywords
	let g:zephyr#Keywords += [{ 'word': item}]
endfor
